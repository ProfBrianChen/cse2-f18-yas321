//////////////////////
//Yanlin Shen, yas321
//CSE002-210
//9/10/2018
//////////////////////

public class Arithmetic{
  public static void main (String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numbShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belts
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //--------------------------------------------------
    
    //Calculating the total price of each products
    double totalCostOfPants = numPants * pantsPrice;
    
    double totalCostOfShirts = numbShirts * shirtPrice;
    
    double totalCostOfBelts = numBelts * beltCost;
    
    //Display the total price of each products    
    System.out.println("The total price of pants is " + totalCostOfPants);
    
    System.out.println("The total price of shirts is " + totalCostOfShirts);
    
    System.out.println("The total price of belts is " + totalCostOfBelts);
    
    //Calculating the total price of all three products
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    //Display the total price of all three products
    System.out.println("The total price is " + totalCost);
    
    //Calculating the tax of each products
    double taxOnPants = paSalesTax * totalCostOfPants;
    
    double taxOnShirts = paSalesTax * totalCostOfShirts;
    
    double taxOnBelts = paSalesTax * totalCostOfBelts;
    
    //Display the tax of each products in two decimals
    System.out.printf("The tax on pants is " + "%.2f\n", taxOnPants);
    
    System.out.printf("The tax on shirts is " + "%.2f\n", taxOnShirts);
    
    System.out.printf("The tax on belts is " + "%.2f\n", taxOnBelts);
    
    //Calculating the total tax of all
    double totalTax = taxOnPants + taxOnShirts + taxOnBelts;
    
    //Display the total tax of all
    System.out.printf("The total tax is " + "%.2f\n", totalTax);
    
    //Calculating the total payment
    double totalPaid = totalTax + totalCost;
    
    //Display the total payment
    System.out.printf("The total paid is " + "%.2f\n", totalPaid);
    
  }
}
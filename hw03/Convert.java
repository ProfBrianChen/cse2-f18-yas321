/////////////////////////
//Yanlin Shen yas321
//CSE002-210
//9/17/2018

import java.util.Scanner;//import Scanner method

public class Convert{ //common settings for java clss
  public static void main (String arsg[]){
    
    //define variables and constants
    double affectedArea;
    double rainfulAmount;
    double rainQuantity;
    final double gallonToCubicmiles = 9.0817e-13; // 1 gallon equals 9.0817e-13
    final int gallons = 27154; //1 inch of rain on 1 acres equals 27154 
    double areaInGallon;
    double areaInCubicmiles;
    
    //define Scanner method
    Scanner scan = new Scanner(System.in);
    
    //Asking for input of areas
    System.out.print("Enter the area affected by rains in acres: ");
    
    affectedArea = scan.nextDouble();
    
    //Asking for input of rains amount
    System.out.print("Enter the amount of rainfalls in inches: ");
    
    rainfulAmount = scan.nextDouble();
    
    //calculating into gallons
    areaInGallon = affectedArea * rainfulAmount * gallons;
    //change gallons to cubic miles
    areaInCubicmiles = areaInGallon * gallonToCubicmiles ;
    
    //print out outputs
    System.out.println("The area affected by rains: " + affectedArea);
    
    System.out.println("The amount of rainfalls: " + rainfulAmount);
    
    System.out.println("The amount of rainfalls in cubic miles: " + areaInCubicmiles);
    
  }
    }
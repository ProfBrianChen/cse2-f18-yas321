////////////////////
//Yanlin Shen yas321
//CSE002-211
//9/17/2018

import java.util.Scanner; //import Scanner method

public class Pyramid{ //common settings for java class

  public static void main (String args[]){
    
    //define variables
    double sideLength;
    double height;
    double volume;
    
    Scanner scan = new Scanner(System.in);//define Scanner method
    
    //Asking for side length
    System.out.print("Enter the side length of pyramid: ");
    sideLength = scan.nextDouble();
    
    //Asking for height
    System.out.print("Enter the height of pyramid: ");
    height = scan.nextDouble();
    
    //square the side length into base area
    double baseArea = Math.pow(sideLength, 2.0);
    
    //the equation of pyramid volume
    volume =  baseArea * height / 3;
    
    //print out outputs
    System.out.println("The volume of the pyramid is: " + volume);
    
  }
}
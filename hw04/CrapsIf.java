/////////////////
//CSE002-210
//Yanlin Shen
//9/25/2018
////////////////


import java.util.Scanner;//import the Scanner mod

//Common settings for every java programs
public class CrapsIf{ 
  public static void main (String args[]){
    
    Scanner scan = new Scanner(System.in);//load Scanner mod
    
    //setting variables
    int decision;
    int num1=1;
    int num2=1;
    String slang = "";
    
    //noting user to choose the methods of inputting
    System.out.println("Please choose the number in fornt of each way for inputting the dice numbers: 1.Random gennerate 2.Personal assign");
    decision = scan.nextInt();
    
    //gennerate random dice number
    int randomNum1 = (int)(Math.random()*6)+1;
    int randomNum2 = (int)(Math.random()*6)+1;
    
    //determin the first method
    if(decision == 1){
      //make sure random numbers are equal to determing number
      num1 = randomNum1;
      num2 = randomNum2;
    }
    //determin the second method
    else if(decision == 2){
      
      //inputing the two assigned numbers
      System.out.println("Please enter the number of first dice: ");
      int assignNum1 = scan.nextInt();
      System.out.println("Please enter the number of second dice: ");
      int assignNum2 = scan.nextInt();
      //make sure assigned numbers are equal to determing numbers
      num1 = assignNum1;
      num2 = assignNum2;
      //restrict the range of assigned numbers
      if(num1>6 || num2 >6){
        System.out.println("Please enter the number in range between 1 to 6.");
      }
    }
    
    //restrcit the range of choosing method
    else{
      System.out.println("Wrong number for input method.");
    }
    
    //base on the determing number to name the dice 
    if(num1==1&&num2==1){
      slang = "Sanke Eyes";
    }
    else if(num1==2&&num2==2){
      slang = "Hard Four";
    }
    if(num1==3&&num2==3){
      slang = "Hard Six";
    }
    if(num1==4&&num2==4){
      slang = "Hard Four";
    }
    if(num1==5&&num2==5){
      slang = "Hard Tem";
    }
    if(num1==6&&num2==6){
      slang = "Boxcars";
    }
    
    if(num1==1 && num2==2){
      slang = "Ace Deuce";
    }
    if(num1==1 && num2==3){
      slang = "Easy four";
    }
    if(num1==1 && num2==4){
      slang = "Fever five";
    }
    if(num1==1 && num2==5){
      slang = "Easy six";
    }
    if(num1==1 && num2==6){
      slang = "Seven out";
    }
    
    if(num1==2 && num2==1){
      slang = "Ace Deuce";
    }
    if(num1==2 && num2==6){
      slang = "Easy Eight";
    }
    if(num1==2 && num2==3){
      slang = "Fever five";
    }
    if(num1==2 && num2==4){
      slang = "Easy six";
    }
    if(num1==2 && num2==5){
      slang = "Seven out";
    }
    
    if(num1==3 && num2==1){
      slang = "Easy four";
    }
    if(num1==3 && num2==2){
      slang = "Easy five";
    }
    if(num1==3 && num2==6){
      slang = "Nine";
    }
    if(num1==3 && num2==4){
      slang = "Seven out";
    }
    if(num1==3 && num2==5){
      slang = "Easy eight";
    }
    
    if(num1==4 && num2==1){
      slang = "Fever five";
    }
    if(num1==4 && num2==2){
      slang = "Easy six";
    }
    if(num1==4 && num2==3){
      slang = "Seven out";
    }
    if(num1==4 && num2==5){
      slang = "Nine";
    }
    if(num1==4 && num2==6){
      slang = "Easy ten";
    }
    
    if(num1==5 && num2==1){
      slang = "Easy six";
    }
    if(num1==5 && num2==2){
      slang = "Seven out";
    }
    if(num1==5 && num2==3){
      slang = "Easy eight";
    }
    if(num1==5 && num2==4){
      slang = "Nine";
    }
    if(num1==5 && num2==6){
      slang = "Yo-leven";
    }
    
    if(num1==6 && num2==1){
      slang = "Seven out";
    }
    if(num1==6 && num2==2){
      slang = "Easy eight";
    }
    if(num1==6 && num2==3){
      slang = "Nine";
    }
    if(num1==6 && num2==4){
      slang = "Easy ten";
    }
    if(num1==6 && num2==5){
      slang = "Yo-leven";
    }
    if(num1<=6 && num2 <=6){
      
      //print the results
       System.out.println("The dice is: " + slang);
    }
  }
}
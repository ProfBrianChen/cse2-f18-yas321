/////////////////
//CSE002-210
//Yanlin Shen
//9/25/2018
////////////////


import java.util.Scanner;//import the Scanner mod

//Common settings for every java programs
public class CrapsSwitch{
  public static void main (String args[]){
    
    Scanner scan = new Scanner(System.in);//load Scanner mod
    String slang = "";
    
    //noting user to choose the methods of inputting
    System.out.println("Please enter 1 for random dice number, enter 2 for assigned dice number.");
    int decision = scan.nextInt();
    
    //switch decision into to methods
    switch(decision){
        
    //determin the first method    
    case 1:
        //gennerate random dice number
        int dice1 = (int)(Math.random()*6)+1;
        int dice2 = (int)(Math.random()*6)+1;
        
        //The number shown on the two dice
        int diceNum = dice1 + dice2;
        //The relations between the two random numbers
        int diceDeterminant = dice1 - dice2;
        
        switch(diceDeterminant){
            
            //If dice numbers are equal
          case 0: 
                 switch(diceNum){
                   case 2: slang = "Sneak Eyes";
                     break;
                   case 4: slang = "Hard four";
                     break;
                   case 6: slang = "Hard six";
                     break;
                   case 8: slang = "Hard eight";
                     break;
                   case 10: slang = "Hard ten";
                     break;
                   case 12: slang = "Boxcars";
                     break;
                   default: slang = "Invalid message";
                     break;
                 }
            break;
            
            //If dice number are not equal
          default:
                  switch(diceNum){
                   case 2: slang = "Sneak Eyes";
                     break;
                   case 3: slang = "Ace Deuce";
                     break;
                   case 4: slang = "Easy four";
                      break;
                    case 5: slang ="Fever five";
                      break;
                    case 6: slang = "Easy six";
                      break;
                    case 7: slang = "Seven out";
                      break;
                    case 8: slang = "Easy eight";
                      break;
                    case 9: slang = "Nine";
                      break;
                    case 10: slang = "Easy ten";
                      break;
                    case 11: slang = "Yo-leven";
                      break;
                    default: slang = "Invalid message";
                      break;
        }
            break;
      }
        break;
        
      //determin the second method  
      case 2:
        //inputing the two assigned numbers
        System.out.println("Please enter the number of first dice: ");
       int diceNum1=scan.nextInt();
        System.out.println("Please enter the number of second dice: ");
       int diceNum2=scan.nextInt();
        //The number shown on the two dice
       int diceTotal = diceNum1 + diceNum2;
        //The relations between the two random numbers
       int diceDeterminant2 = diceNum1 - diceNum2;
        
        switch(diceDeterminant2){
            
            //If dice numbers are equal
          case 0: 
                 switch(diceTotal){
                   case 2: slang = "Sneak Eyes";
                     break;
                   case 4: slang = "Hard four";
                     break;
                   case 6: slang = "Hard six";
                     break;
                   case 8: slang = "Hard eight";
                     break;
                   case 10: slang = "Hard ten";
                     break;
                   case 12: slang = "Boxcars";
                     break;
                   default: slang = "Invalid message";
                     break;
                 }
            break;
            
            //If dice number are not equal
          default:
                  switch(diceTotal){
                   case 2: slang = "Sneak Eyes";
                     break;
                   case 3: slang = "Ace Deuce";
                     break;
                   case 4: slang = "Easy four";
                      break;
                    case 5: slang ="Fever five";
                      break;
                    case 6: slang = "Easy six";
                      break;
                    case 7: slang = "Seven out";
                      break;
                    case 8: slang = "Easy eight";
                      break;
                    case 9: slang = "Nine";
                      break;
                    case 10: slang = "Easy ten";
                      break;
                    case 11: slang = "Yo-leven";
                      break;
                      //restrict the range of assigned numbers
                    default: slang = "Invalid message";
                      break;
        }
            break;
      }
        break;
        //restrcit the range of choosing method
      default: slang = "Invalid message";
        break;
    }
    System.out.println("The dice of your pick is " + slang);
  }
}
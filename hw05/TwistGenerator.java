//CSE011-210
//Yanlin Shen
//10/10/2018

//import scanner
import java.util.Scanner;

//general settings
public class TwistGenerator{
  
  public static void main (String args[]){
    
    //declare Scanner
    Scanner scan = new Scanner(System.in);
    
    //declare variable
    int length;
    int row1;
    int row2;
    int row3;
    int determinant = 0;
    String junk = "";
    
    System.out.println("Please enter an positive integer: ");
    
    //detect input correctness
    do{
    while(!scan.hasNextInt()){
      System.out.println("Please enter the correct number.");
      junk = scan.nextLine();
    }
    length = scan.nextInt();
    if(length >=0){
      determinant = 1;
    }
    else{
      System.out.println("Please enter the correct number.");
      junk = scan.nextLine();
    }
    }while(determinant != 1);
    
  System.out.println("the length is:" + length);
  
  //print out first row
  for(row1 = 0;row1 <= length / 3;row1++){
    if(row1==length / 3){
      if(length % 3 == 0){
        break;
      }
      else if(length % 3 == 1){
      System.out.print("\\");
      break;
      }
      else if(length % 3 == 2){
      System.out.print("\\ ");
      break;
      }
      else{
        System.out.print("error");
      }}
    System.out.print("\\ /");
    }
    System.out.println();
    
    //print out second row
    for(row2 = 0;row2 <= length / 3;row2++){
    if(row2 == length / 3){
      if(length % 3 == 0){
        break;
      }
      else if(length%3 ==1){
      System.out.print(" ");
      break;
      }
      else if(length % 3 == 2){
      System.out.print(" X");
      break;
      }
      else{
        System.out.print("error");
      }}
      System.out.print(" X ");}
    System.out.println();
    
    //print out third row
    for(row3 = 0;row3 <= length / 3;row3++){
    if(row3 == length / 3){
      if(length % 3 == 0){
        break;
      }
      else if(length % 3 == 1){
      System.out.print("/");
      break;
      }
      else if(length % 3 == 2){
      System.out.print("/ ");
      break;
      }
      else{
        System.out.print("error");
      }}
      System.out.print("/ \\");}
    
  }
  }
//CSE002-210
//Yanlin Shen
//yas321
//10/21/2018


import java.util.Scanner;//import Scanner method

public class EncryptedX{//common settings for class
  public static void main (String args[]){
    
    
    Scanner scan = new Scanner(System.in);//declare Scanner
    //declare variables
    int input;
    int row;
    int line;
    int size;
    String junk = "";
    int determinant = 0;
    //as for the size of the square
    System.out.println("Please enter the number between 0 to 100: ");
    
  do{
  while(!scan.hasNextInt()){
     //retype the input when it is incorrect as an integer
    System.out.println("Please enter the correct number: ");
    junk = scan.nextLine();
  }
  input = scan.nextInt();
   //pass the loop when input is correct
  if(input<=100&&input>=0){
     determinant = 1;
  }
  //retype the input when it is not in the range
  else{
    System.out.println("Please enter the correct number: ");
    junk = scan.nextLine();
  }
  }while(determinant != 1);
  
  //outer loop for controling the row
  for(row = 1; row <= input+1; row++){
    //inner loop for controling the line
    for(line =  0; line <= input; line++){
      //print the first space in one line
      if(line == row-1){
      System.out.print(" ");
      }
      //print the second space in one line
      else if(line == (input-row+1)){
         System.out.print(" ");
      }
      //print * to fulfill the line
      else{
      System.out.print("*");
      }
  }
    //change lines
    System.out.println();
  }
}
}
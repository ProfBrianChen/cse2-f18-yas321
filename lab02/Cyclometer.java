///////////////////
//Yanlin Shen yas321
//CSE002-210
//9.5.2018

public class Cyclometer{
  //main method required for every java program
  public static void main(String args[]){
    //Creating variables
    int secsTrip1 = 480;//time that trip1 took
    int secsTrip2 = 3220;//time that trip2 took
    int countsTrip1 = 1561;//wheel counts for trip1
    int countsTrip2 = 9037;//wheel counts for trip2
    final double wheelDiameter = 27.0;//the diameter of wheel
    final double PI = 3.14159;//the constant of pi
    final double feetPerMile = 5280;//the unit covert from foot to mile
    final double inchesPerFoot = 12;//the unit covert from inch to mile
    final double secondsPerMinute = 60;//the unit covert from second to minute
    double distanceTrip1, distanceTrip2, totalDistance;//set variables
    
    //calcuating the time for both trips in minuts and the wheel counts
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts. ");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts. ");
    
    //give distance in inch
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    
    distanceTrip1 /= inchesPerFoot * feetPerMile;//give distance in mile 
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;//do exact the same process as trip1
    totalDistance = distanceTrip1 + distanceTrip2;//calculate the total distance from trip1 and trip2
    //print out the data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles. ");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles. ");
    System.out.println("The total distance was " + totalDistance + " miles. ");
    
  }
}
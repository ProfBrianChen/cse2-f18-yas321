////////////////////
//Yanlin Shen yas321
//CSE002-210
//9/12/2018

import java.util.Scanner; //import java input mod



public class Check{
  
  public static void main (String args[]){ //main settings for every java program
    
    Scanner scan = new Scanner(System.in); //initialize scanner
    
    System.out.print("Enter the original cost of the check in the form XX.XX: "); //Ask for total cost
    
    double checkCost = scan.nextDouble(); //define variables by scanning inputs
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form XX): "); // Ask for tip percentage
    
    double tipPercent = scan.nextDouble(); //define variables
    
    tipPercent /= 100; //convert the percentage into a decimal value

    System.out.print("Enter the number of people who went out for dinner: "); 
    
    int numPeople = scan.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }
}
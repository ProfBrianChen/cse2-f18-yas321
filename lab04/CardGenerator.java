/////////////////
//CSE002-210
//Yanlin Shen
//9/19/2018

//Common settings for java program
public class CardGenerator{
  public static void main (String[] args){
   //define the card number
   String identity = ""; 
   //define the card colors
   String color = "";
    //generate random numbers
   int cardNum = (int)(Math.random()*52)+1;    
    //define the range of colors
   if (cardNum >= 1 && cardNum <= 13){
     color = "diamonds";
     
   }
    else if(cardNum >= 14 && cardNum <= 26){
      color = "clubs";
    }
    else if(cardNum>= 27 && cardNum<= 39){
      color = "hearts";
    }
    else if(cardNum >= 40 && cardNum<= 52){
      color = "spades";
    }
    //using modulus to calculate the numbers
    cardNum = cardNum%13;
    //switch the numbers to card identities
    switch(cardNum){
      case 1: identity = "Ace";
        break;
        case 2: identity = "2";
        break;
        case 3: identity = "3";
        break;
        case 4: identity = "4";
        break;
        case 5: identity = "5";
        break;
        case 6: identity = "6";
        break;
        case 7: identity = "7";
        break;
        case 8: identity = "8";
        break;
        case 9: identity = "9";
        break;
        case 10: identity = "10";
        break;
        case 11: identity = "Jack";
        break;
        case 12: identity = "Queen";
        break;
        case 0: identity = "King";
        break;
        default: identity = "invalid Message";
        break;
    }
    //print out the results
    System.out.println("You picked the " + identity + " of " + color);
    
    
  }
}
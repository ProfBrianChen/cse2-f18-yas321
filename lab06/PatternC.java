//CSE02-210
//Yanlin Shen
//Prof.Kalafut
//10.16.2018

import java.util.Scanner;//import Scanner

public class PatternC{
public static void main (String args[]){//common settings of class
  
  Scanner scan = new Scanner(System.in);//declare Scanner
  //declare and initialize variables
  int input;
  int numRows = 1;
  int determinant = 0;
  int numLines;
  String junk = "";
  String result = "";
  
  //ask the user for input
  System.out.println("Please enter you number as an integer between 1 and 10: ");
  //detect input correctness
  do{
  while(!scan.hasNextInt()){
    //retype the input when it is incorrect as an integer
    System.out.println("Please enter the correct number: ");
    junk = scan.nextLine();
  }
  input = scan.nextInt();
  //pass the loop when input is correct
  if(input<=10&&input>=1){
     determinant = 1;
  }
  //retype the input when it is not in the range
  else{
    System.out.println("Please enter the correct number: ");
  }
  }while(determinant != 1);
  
  //print out the input
  System.out.println("The number you have entered is: " + input);
  
  //for loop to create the pyramid
  for(numRows = 1; numRows <= input; numRows++){
    result = numRows + result;
      System.out.printf("%11s\n",result);
}
}
}